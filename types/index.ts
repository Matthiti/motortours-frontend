import { Strapi4ResponseMany, Strapi4ResponseSingle } from '@nuxtjs/strapi/dist/runtime/types';

export interface Strapi4ResponseNull {
  data: null;
}

export interface File {
  name: string;
  url: string;
}

export interface MotorCycle {
  name: string;
  year: number;
  image: Strapi4ResponseSingle<File>;
}

export interface Tour {
  slug: string;
  title: string;
  content: string;
  coverImage: Strapi4ResponseSingle<File>;
  images: Strapi4ResponseMany<File> | Strapi4ResponseNull;
  gpx: Strapi4ResponseSingle<File> | Strapi4ResponseNull;
  riddenOnDate: string;
  lengthKm: number;
  lengthMinutes: number;
  elevation: Number;
  roadTypes?: string[];
  roadQuality: number;
  curvature: number;
  motorcycles: Strapi4ResponseMany<MotorCycle>;
}

export interface FeaturedTour {
  index: number;
  tour: Strapi4ResponseSingle<Tour>;
}
