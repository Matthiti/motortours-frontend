# Motorritme Frontend

This is the frontend of https://motorritme.nl.

## Requirements

- node 18
- npm

## Install

```sh
npm install
```

## Run

```sh
npm run dev
```

## Build

```sh
npm run build
```
