FROM node:18-alpine AS builder

ENV NODE_ENV=production

WORKDIR /app

# Install dependencies
COPY package.json package-lock.json ./
RUN npm install --production=false

COPY . .
RUN npm run build

FROM node:18-alpine

ENV PORT=3000

WORKDIR /app

COPY --from=builder /app/.output .output

RUN chown -R node:node /app
USER node
EXPOSE 3000

CMD ["node", ".output/server/index.mjs"]
