// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    '@nuxtjs/strapi',
    '@nuxtjs/tailwindcss',
    '@nuxtjs/eslint-module',
    'nuxt3-leaflet',
    'dayjs-nuxt'
  ],
  dayjs: {
    locales: ['en', 'nl'],
    plugins: ['customParseFormat']
  },
  app: {
    head: {
      title: 'Motorritme',
      htmlAttrs: {
        lang: 'nl'
      },
      charset: 'utf-8',
      viewport: 'width=device-width, initial-scale=1',
      meta: [
        { hid: 'description', name: 'description', content: 'Motorritme' },
        { name: 'format-detection', content: 'telephone=no' }
      ]
    }
  }
});
