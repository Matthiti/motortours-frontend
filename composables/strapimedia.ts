export const useStrapiMediaWithSize = (path: string, size: string) => {
  const pathComponents = path.split('/');
  pathComponents[pathComponents.length - 1] = `${size}_${
    pathComponents[pathComponents.length - 1]
  }`;
  return useStrapiMedia(pathComponents.join('/'));
};
