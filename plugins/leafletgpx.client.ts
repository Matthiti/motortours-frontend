// eslint-disable-next-line import/namespace
import * as L from 'leaflet-gpx';

export default defineNuxtPlugin(() => {
  return {
    provide: {
      L
    }
  };
});
